# jm-cancel-token

这是一个用于，阻断请求的库

## API

### getUrl

获取 URL

```js
CancelToken.getUrl(config);
```

### addPending

添加请求

```js
CancelToken.addPending(config);
```

### removePending

添加请求

```js
CancelToken.removePending(config);
```

### clearPending

清空 pending 中的请求（在路由跳转时调用）

```js
CancelToken.clearPending();
```
