import { defineConfig } from "vite";
import PACK from "./package.json";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";

const { name } = PACK;

export default defineConfig((mode) => {
  const isDev = mode === "development";
  return {
    plugins: [vue()],
    build: isDev
      ? null
      : {
          lib: {
            entry: resolve(__dirname, "./src/main.js"),
            name: name.toLocaleUpperCase(),
            // the proper extensions will be added
            fileName: name,
          },
          rollupOptions: {
            // 确保外部化处理那些你不想打包进库的依赖
            external: ["vue", "axios", "qs"],
          },
        },
  };
});
