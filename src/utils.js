export function getRequestParams(url) {
  var theRequest = new Object();
  if (url.indexOf("?") != -1) {
    var strs = url.substr(1).split("?")[1].split("&");
    for (var i = 0; i < strs.length; i++) {
      let index = strs[i].indexOf("=");
      theRequest[strs[i].substr(0, index)] = unescape(
        strs[i].substr(index + 1)
      );
    }
  }
  return theRequest;
}
